# README #

 This program runs on the "Arduino Relay Controller" as part of the Shelf Conveyor Machine for Fort McMurray. It allows the Touch Screen Interface to communicate and control the Conveyor through the Electrical Panel.

 It was written for Globacore by Nick Stedman <nick@moti.ph>, April, 2015.

---
 
 This program runs on an Arduino Mega...only pins need changing to run on another Arduino variant.

 It is recommended that the USB connection between the touch screen and the Electrical Panel is disconnected after powering off. Only reconnect after the Panel is powered up again. Alternatively, powering up the touch screen after the panel is powered up may also work, but is untested.

 On start-up the conveyor is disabled. It won't move until the enable button is pressed.

 Once enabled, the conveyor processes any commands available on the Serial port, or will wait until a command is received.

---
 
## Commands: ##

 *Commands are sent classified by the (public) mode that they fall into:*
 
### RUN MODE (default mode) commands: ###
 
*  '1' - goto shelf 1
*  '2' - goto shelf 2 
*  '3' - goto shelf 3
*  '4' - goto shelf 4
*  '5' - goto shelf 5
*  '6' - goto shelf 6
*  '7' - goto shelf 7
*  '8' - goto shelf 8
*  '9' - goto shelf 9
*  ':' - goto shelf 10
*  ';' - goto shelf 11
*  '<' - goto shelf 12
 
 In each case the conveyor will take the shortest route from it's current position to the destination.
 The current shelf position is reported back over Serial, when it changes.
 
 
 ---
 
### PRIMARY_CONTROL_MODE commands: ###
 
*  'a' - SET_DOWN_DIRECTION, (doesn't initiate movement)
*  'b' - SET_UP_DIRECTION, (doesn't initiate movement)
*  'c' - MOVE_SLOW, (initiates movement)
*  'd' - MOVE_FAST, (initiates movement)
*  'e' - MOVE_STOP, (initiates movement)
*  'f' - NA, reserved for future use
 
 The above commands safely set the states of up to several outputs at a time.
 
 
*  'g' - ADVANCED_START_ON
*  'h' - ADVANCED_START_OFF
*  'i' - ADVANCED_DIRECTION_UP
*  'j' - ADVANCED_DIRECTION_DOWN
*  'k' - ADVANCED_SLOW_ON 
*  'l' - ADVANCED_SLOW_OFF
*  'm' - ADVANCED_FAST_ON
*  'n' - ADVANCED_FAST_OFF
*  'o' - ADVANCED_ENABLE_LIGHT_ON
*  'p' - ADVANCED_ENABLE_LIGHT_OFF
*  'q' - ADVANCED_READ_SHELF_SENSOR
*  'r' - ADVANCED_READ_PENDANT_SENSOR
*  's' - ADVANCED_READ_ABS_ZERO_SENSOR
*  't' - ADVANCED_READ_ENABLE_BUTTON
*  'u' - DEBUG_ON, starts printing verbose statements out Serial port.
*  'v' - DEBUG_OFF (default)
 
 Advanced controls manipulate the individual connection lines to the PLC.
 Use with caution, as it is possible to generate conflicting states to unknown effect
 eg. ADVANCED_FAST_ON and ADVANCED_SLOW_ON create conflicting messages to PLC. 
 
---
 
### SEEK_HOME_MODE commands: ###

  'w' - SEEK_HOME, Rotate Upwards until HOME_IN sensor is found.

 Use this command to recalibrate the machine, eg. after working in PRIMARY_CONTROL_MODE.