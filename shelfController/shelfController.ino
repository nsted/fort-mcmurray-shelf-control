/************************************************************************************************************************

This program runs on the "Arduino Relay Controller" as part of the Shelf Conveyor Machine for Fort McMurray. It allows the Touch Screen Interface to communicate and control the Conveyor through the Electrical Panel.

It was written for Globacore by Nick Stedman nick@moti.ph, April, 2015.

This program runs on an Arduino Mega...only pins need changing to run on another Arduino variant.

It is recommended that the USB connection between the touch screen and the Electrical Panel is disconnected after powering off. Only reconnect after the Panel is powered up again. Alternatively, powering up the touch screen after the panel is powered up may also work, but is untested.

On start-up the conveyor is disabled. It won't move until the enable button is pressed.

Once enabled, the conveyor processes any commands available on the Serial port, or will wait until a command is received.
Commands:

Commands are sent classified by the (public) mode that they fall into:
RUN MODE (default mode) commands:

    '1' - goto shelf 1
    '2' - goto shelf 2
    '3' - goto shelf 3
    '4' - goto shelf 4
    '5' - goto shelf 5
    '6' - goto shelf 6
    '7' - goto shelf 7
    '8' - goto shelf 8
    '9' - goto shelf 9
    ':' - goto shelf 10
    ';' - goto shelf 11
    '<' - goto shelf 12

In each case the conveyor will take the shortest route from it's current position to the destination. The current shelf position is reported back over Serial, when it changes.
PRIMARY_CONTROL_MODE commands:

    'a' - SET_DOWN_DIRECTION, (doesn't initiate movement)
    'b' - SET_UP_DIRECTION, (doesn't initiate movement)
    'c' - MOVE_SLOW, (initiates movement)
    'd' - MOVE_FAST, (initiates movement)
    'e' - MOVE_STOP, (initiates movement)
    'f' - NA, reserved for future use

The above commands safely set the states of up to several outputs at a time.

    'g' - ADVANCED_START_ON
    'h' - ADVANCED_START_OFF
    'i' - ADVANCED_DIRECTION_UP
    'j' - ADVANCED_DIRECTION_DOWN
    'k' - ADVANCED_SLOW_ON
    'l' - ADVANCED_SLOW_OFF
    'm' - ADVANCED_FAST_ON
    'n' - ADVANCED_FAST_OFF
    'o' - ADVANCED_ENABLE_LIGHT_ON
    'p' - ADVANCED_ENABLE_LIGHT_OFF
    'q' - ADVANCED_READ_SHELF_SENSOR
    'r' - ADVANCED_READ_PENDANT_SENSOR
    's' - ADVANCED_READ_ABS_ZERO_SENSOR
    't' - ADVANCED_READ_ENABLE_BUTTON
    'u' - DEBUG_ON, starts printing verbose statements out Serial port.
    'v' - DEBUG_OFF (default)

Advanced controls manipulate the individual connection lines to the PLC. Use with caution, as it is possible to generate conflicting states to unknown effect eg. ADVANCED_FAST_ON and ADVANCED_SLOW_ON create conflicting messages to PLC.
SEEK_HOME_MODE commands:

'w' - SEEK_HOME, Rotate Upwards until HOME_IN sensor is found.

Use this command to recalibrate the machine, eg. after working in PRIMARY_CONTROL_MODE.
 
************************************************************************************************************************/



#include <EEPROM.h>

const boolean ONLY_UP = true;    // true if you want to go only in up direction

enum modes {
  DISABLE_MODE,
  RUN_MODE, 
  DEBUG_MODE,
  PRIMARY_CONTROL_MODE,
  SEEK_HOME_MODE
};

const char COMMANDS_BEGIN = ( 'a' - 1 );
const char SET_DOWN_DIRECTION = 'a';
const char SET_UP_DIRECTION = 'b'; 
const char MOVE_SLOW = 'c'; 
const char MOVE_FAST = 'd'; 
const char MOVE_STOP = 'e';
const char NA = 'f';
const char ADVANCED_START_ON = 'g'; 
const char ADVANCED_START_OFF = 'h'; 
const char ADVANCED_DIRECTION_UP = 'i'; 
const char ADVANCED_DIRECTION_DOWN = 'j'; 
const char ADVANCED_SLOW_ON = 'k'; 
const char ADVANCED_SLOW_OFF = 'l'; 
const char ADVANCED_FAST_ON = 'm'; 
const char ADVANCED_FAST_OFF = 'n'; 
const char ADVANCED_ENABLE_LIGHT_ON = 'o'; 
const char ADVANCED_ENABLE_LIGHT_OFF = 'p'; 
const char READ_SHELF_SENSOR = 'q'; 
const char READ_PENDANT_SENSOR = 'r';
const char READ_HOME_SENSOR = 's';
const char READ_ENABLE_BUTTON = 't';
const char DEBUG_ON = 'u';
const char DEBUG_OFF = 'v';
const char COMMANDS_END = 'w';

const char SEEK_HOME = 'w';

// input pins
const int HOME_IN = A0;
const int SHELF_IN = A1;
const int PENDANT_IN = A2;
const int ENABLE_BUTTON_IN = A3;

// output pins
//const int DIRECTION_OUT = 40;
//const int START_OUT = 42;
//const int SLOW_OUT = 46;
//const int FAST_OUT = 44;
//const int ENABLE_LIGHT_OUT = 48;

const int DIRECTION_OUT = 2;
const int START_OUT = 3;
const int SLOW_OUT = 4;
const int FAST_OUT = 5;
const int ENABLE_LIGHT_OUT = 6;

// machine specific parameters
const int SHELF_TOTAL = 12;
const int PENDANTS_PER_SHELF = 6;
const int PENDANT_TOTAL = SHELF_TOTAL * PENDANTS_PER_SHELF;
const int SHELF_VALUE = 1;
const int SHELF_THRESH = SHELF_VALUE;
const int PENDANTS_THRESH = PENDANTS_PER_SHELF;
const int UNITS_PER_SHELF = PENDANTS_PER_SHELF;

// EEPROM registers 
const int SHELF_DESTINATION_REGISTER = 0;
const int SHELF_POSITION_REGISTER = 1;
const int PENDANT_POSITION_REGISTER = 2;
const int DIRECTION_REGISTER = 3;

// speeds
enum vel {
  NONE,
  STOP,
  SLOW,
  FAST
};

// keywords
const boolean DISABLE = HIGH;
const boolean ENABLE = LOW;
const boolean UP = LOW;
const boolean DOWN = HIGH;


int mode = DISABLE_MODE;

int shelfDestination;
int shelfPosition;
int pendantPosition;
int masterGap;
boolean currentDirection = UP;

int lastShelfDestination;
int lastShelfPosition;
int lastPendantPosition;
int lastGap;
boolean lastDirection = UP;

boolean debug = false;





/************************************************
 * 
 * Let's get going...
 * 
 ************************************************/

void setup() {

  setupIo();                                                // Setup the IO pins
  Serial.begin(9600);                                       // start Serial port
//  if( debug ){
//    Serial.println("start");
//  }
  loadData( shelfDestination, shelfPosition, pendantPosition, currentDirection );

}



void loop() {
  if( false ){                        // Don't move if not allowed (enable button is off) ... !!!disabled to never execute!!!
//  if( isMachineEnabled() == false ){                        // Don't move if not allowed (enable button is off)
    digitalWrite( ENABLE_LIGHT_OUT, DISABLE );
    stopMoving();
  }
  else {
    digitalWrite( ENABLE_LIGHT_OUT, ENABLE );

    int data = checkForData();                              // Otherwise check for new data
    if( data >= 0 ) {  
      mode = modeUpdate( data );                            // if there's some, determine the machine mode, and run corresponding routines
    }

    switch( mode ) {                                      

    case DISABLE_MODE:                                    // disable...have to receive a new command to begin moving
      stopMoving();        
      break;

    case PRIMARY_CONTROL_MODE:                            // primary controls are key controls over the behaviour of the machine
      primaryControl( data );  
      break;      

    case RUN_MODE:                                        // RUN_MODE is standard behaviour. Send a shelf, and go to it.
      {
        static int lastShelf;
        int shelf = shelfValidate( data );
        if( shelf ){
          shelfDestination = shelf;
          run( shelfDestination );
          lastShelf = shelfDestination;
        }
        else {
          run( lastShelf );
        }
        break;    
      }

    case SEEK_HOME_MODE:                                  // Recalibrate the machine, by moving until we hit the Home Sensor
      { 
        goHome();
        break; 
      }  
    }
  }

  // check the sensors, and update position,
  // how this is used depends on the mode.
  
  shelfPosition = shelfPositionUpdate( shelfPosition );
  pendantPosition = pendantPositionUpdate( pendantPosition );  
    
  //  confirm( shelfPosition, pendantPosition );
  saveChanges( shelfDestination, shelfPosition, pendantPosition, currentDirection );     
  reportShelfPosition( shelfPosition );
  // reportShelfFromPendants()  
  
  if( debug ) {
    debugShelfData( shelfDestination, shelfPosition, pendantPosition, currentDirection, masterGap );
  }  

  delay(150);                                                // Wait a bit...to prevent duplicate sensor triggers

}



/************************************************
 * 
 * Main routine to Move the Shelves
 * 
 ************************************************/


void run( int shelfDest ) { 

  static int lastShelfDest; 
  static int lastPos = shelfPosition;

  // if destination has changed, update the control variables
  if( shelfDest != lastShelfDest ){
    masterGap = readGap( shelfDest, shelfPosition, SHELF_TOTAL );
    currentDirection = directionUpdate( masterGap, currentDirection );
    
    // offset if direction changed
    int offset = calibrateShelvesForDirectionChange( masterGap, currentDirection );
    shelfPosition += offset;
    
    lastPos = shelfPosition;
    lastShelfDest = shelfDest;
  }

  // if the position has changed, update the control variables
  else if( shelfPosition != lastPos ){
    masterGap = readGap( shelfDest, shelfPosition, SHELF_TOTAL );
    lastPos = shelfPosition;
  }

  // make it go...
  control( masterGap, currentDirection, SHELF_THRESH );                             // Control the direction and speed based on the gap
//  control( masterGap, currentDirection, PENDANTS_THRESH );                          // Control the direction and speed based on the gap  

}








