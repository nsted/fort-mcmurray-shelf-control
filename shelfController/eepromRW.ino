/************************************************
 * 
 * Save and Load from EEPROM in case there's a power out.
 * 
 ************************************************/

void loadData( int& dest, int& shelf, int& pendant, bool& dir ) {

  dest = EEPROM.read( SHELF_DESTINATION_REGISTER );   
  shelf = EEPROM.read( SHELF_POSITION_REGISTER );   
  pendant = EEPROM.read( PENDANT_POSITION_REGISTER );  
  dir = EEPROM.read( DIRECTION_REGISTER ); 
  
  if( debug ){
    Serial.print( "LOADING: " );   
    Serial.print( dest ); 
    Serial.print( ',' );    
    Serial.print( shelf ); 
    Serial.print( ',' );    
    Serial.print( pendant ); 
    Serial.print( ',' );    
    Serial.println( dir );                      
  }
}



void saveChanges( int dest, int shelf, int pendant, int dir ) {

  static int lastShelfDestination = dest;
  static int lastShelfPosition = shelf;
  static int lastPendantPosition = pendant;  
  static int lastDirection = dir;    

  if( dest != lastShelfDestination ){
//    if( debug ){
//      Serial.print( "saving shelf Dest " );   
//      Serial.println( dest );                
//    }
    EEPROM.write( SHELF_DESTINATION_REGISTER, dest );                          // Save the position so we know where we are if we restart for some reason
    lastShelfDestination = dest;
  }

  if( shelf != lastShelfPosition ){
//    if( debug ){      
//      Serial.print( "saving shelf Pos " );    
//      Serial.println( shelf );               
//    }
    EEPROM.write( SHELF_POSITION_REGISTER, shelf );                            // Save the position so we know where we are if we restart for some reason
    lastShelfPosition = shelf;
  }

  if( pendant != lastPendantPosition ){
//    if( debug ){       
//      Serial.print( "saving pendant Pos " );  
//      Serial.println( pendant );             
//    }
    EEPROM.write( PENDANT_POSITION_REGISTER, pendant );                        // Save the position so we know where we are if we restart for some reason
    lastPendantPosition = pendant;
  }    
  if( dir != lastDirection ){
//    if( debug ){ 
//      Serial.print( "saving direction " );   
//      Serial.println( dir );                
//    }
    EEPROM.write( DIRECTION_REGISTER, dir );                          // Save the position so we know where we are if we restart for some reason
    lastDirection = dir;
  }
  
}

