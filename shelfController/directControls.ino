
/************************************************
 * 
 * Set the initial state of the IO lines
 * 
 ************************************************/

void setupIo() {

  // Outputs

  pinMode( DIRECTION_OUT, OUTPUT );                     
  digitalWrite( DIRECTION_OUT, DOWN );    

  pinMode( START_OUT, OUTPUT );                     
  digitalWrite( START_OUT, DISABLE );                 // STOP the machine on startup

  pinMode( SLOW_OUT, OUTPUT );                     
  digitalWrite( SLOW_OUT, DISABLE );   

  pinMode( FAST_OUT, OUTPUT );                     
  digitalWrite( FAST_OUT, DISABLE );  

  pinMode( ENABLE_LIGHT_OUT, OUTPUT );  
  digitalWrite( ENABLE_LIGHT_OUT, DISABLE );


  // Inputs...use internal pullups

  pinMode( HOME_IN, INPUT );                     
  digitalWrite( HOME_IN, HIGH );   

  pinMode( SHELF_IN, INPUT );                     
  digitalWrite( SHELF_IN, HIGH );   

  pinMode( PENDANT_IN, INPUT );                     
  digitalWrite( PENDANT_IN, HIGH );   

  pinMode( ENABLE_BUTTON_IN, INPUT );                     
  digitalWrite( ENABLE_BUTTON_IN, HIGH );     

}  



/************************************************
 * 
 * Reads the Machine Enabled Button
 * 
 ************************************************/


boolean isMachineEnabled() {
  
  static boolean enableState = false;
  
  boolean enableButtonState = ! digitalRead( ENABLE_BUTTON_IN );   
  static boolean lastEnableButtonState = enableButtonState;    

  if( enableButtonState != lastEnableButtonState ) {
    if( enableButtonState == LOW ) {
      enableState = ! enableState;
      
      if( enableState ) {
        if( debug ){
          Serial.println( "machine enabled" );
        }
        digitalWrite( ENABLE_LIGHT_OUT, ENABLE );
      }
      else {
        if( debug ){
          Serial.println( "machine disabled" );
        }    
        digitalWrite( ENABLE_LIGHT_OUT, DISABLE );      
      }      
    }
    
    lastEnableButtonState = enableButtonState;
  }

  return enableState;    

}




/************************************************
 * 
 * Low Level Controls.
 *
 *    Need to Run in SEEK_HOME_MODE after using.
 * 
 ************************************************/
 

void primaryControl( int command ) {  

  switch( command ){

  case SET_DOWN_DIRECTION:
    if( debug ){
      Serial.println("DOWN");
    }
    currentDirection = DOWN;
    setDownDirection();
    break;

  case SET_UP_DIRECTION:
    if( debug ){
      Serial.println("UP");
    }
    currentDirection = UP;
    setUpDirection();
    break;

  case MOVE_SLOW:
    if( debug ){
      Serial.println("SLOW");
    }  
    goSlow();
    break;

  case MOVE_FAST:
    if( debug ){
      Serial.println("FAST");
    }  
    goFast();
    break;

  case MOVE_STOP:
    if( debug ){
      Serial.println("STOP");
    }  
    stopMoving();
    break;
    
  case NA:
    if( debug ){
      Serial.println("NOT AVAILABLE");
    }  
    stopMoving();
    break;  


    // ADVANCED CONTROLS: Direct Manipulation of Pin States
    // !!!!!!!  WARNING, WARNING, WARNING - can create conflicting states...
    // make sure to disable/enable pins in correct sequence. !!!!!
  case ADVANCED_START_ON:
    if( debug ){
      Serial.println("START_OUT ENABLE/LOW");
    }  
    digitalWrite( START_OUT, ENABLE );
    break;

  case ADVANCED_START_OFF:
    if( debug ){
      Serial.println("START_OUT DISABLE/HIGH");
    }  
    digitalWrite( START_OUT, DISABLE );
    break;            

  case ADVANCED_DIRECTION_UP:
    if( debug ){
      Serial.println("DIRECTION_OUT DISABLE/HIGH (UP)");
    }    
    setUpDirection();
    break;

  case ADVANCED_DIRECTION_DOWN:
    if( debug ){
      Serial.println("DIRECTION_OUT ENABLE/LOW (DOWN)");
    }    
    setDownDirection();
    break;      

  case ADVANCED_SLOW_ON:
    if( debug ){
      Serial.println("SLOW_OUT ENABLE/LOW");
    }    
    digitalWrite( SLOW_OUT, ENABLE ); 
    break;

  case ADVANCED_SLOW_OFF:
    if( debug ){
      Serial.println("SLOW_OUT DISABLE/HIGH");
    }    
    digitalWrite( SLOW_OUT, DISABLE ); 
    break;         

  case ADVANCED_FAST_ON:
    if( debug ){
      Serial.println("FAST_OUT ENABLE/LOW");
    }    
    digitalWrite( FAST_OUT, ENABLE ); 
    break;

  case ADVANCED_FAST_OFF:
    if( debug ){
      Serial.println("START_OUT DISABLE/HIGH");
    }    
    digitalWrite( FAST_OUT, DISABLE ); 
    break;      
    
  case ADVANCED_ENABLE_LIGHT_ON:
    if( debug ){
      Serial.println("ENABLE_LIGHT_OUT ENABLE/LOW");
    }    
    digitalWrite( ENABLE_LIGHT_OUT, ENABLE ); 
    break;

  case ADVANCED_ENABLE_LIGHT_OFF:
    if( debug ){
      Serial.println("ENABLE_LIGHT_OUT DISABLE/HIGH");
    }    
    digitalWrite( ENABLE_LIGHT_OUT, DISABLE ); 
    break;       



    // read sensors directly
  case READ_SHELF_SENSOR:
    {
      boolean isShelfThere = ! digitalRead( SHELF_IN );
      if( debug ){
        Serial.print("SHELF_IN = ");
      }          
      Serial.println( isShelfThere );
      break;
    }

  case READ_PENDANT_SENSOR:
    {
      boolean isPendantThere = ! digitalRead( PENDANT_IN );
      if( debug ){
        Serial.print("PENDANT_IN = ");
      }            
      Serial.println( isPendantThere );
      break;
    }

  case READ_HOME_SENSOR:
    {
      boolean isHome = ! digitalRead( HOME_IN );
      if( debug ){
        Serial.print("HOME_IN = ");
      }              
      Serial.println( isHome );
      break;   
    }

  case READ_ENABLE_BUTTON:
    {
      boolean isEnableButtonPushed = ! digitalRead( ENABLE_BUTTON_IN );    
      if( debug ){
        Serial.print("ENABLE_BUTTON_IN = ");
      }                 
      Serial.println( isEnableButtonPushed );
      break;         
    }
    
    
  case DEBUG_ON:
    {
      debug = true;
      Serial.println( "DEBUG_ON" );
      break;
    }
    
  case DEBUG_OFF:
    {
      Serial.println( "DEBUG_OFF" );  
      debug = false;
      break;
    }
    
  }

}


