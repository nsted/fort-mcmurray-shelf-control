/************************************************
 * 
 * Control the Machine based on the gap 
 * 
 ************************************************/

void control( int gap, int dir, int slowThresh ) {

//  static boolean lastDirection = ! currentDirection;
  
  directionControl( dir );
  speedControl( gap, slowThresh );

}



// if the shelf is above the dest, move down 
// if the shelf is below the dest, move up
boolean directionControl( int dir ) {

  digitalWrite( DIRECTION_OUT, dir );

}


boolean setDownDirection(){ 

  digitalWrite( DIRECTION_OUT, DOWN );   
  return DOWN;

}



boolean setUpDirection(){ 

  digitalWrite( DIRECTION_OUT, UP );   
  return UP;

}



void speedControl( int gap, int slowThresh ){

  static vel lastVelocity = NONE;
  vel velocity = STOP;
  boolean atDest = false;

  if( ( gap == 0 ) ){
    velocity = STOP; 

// SHELF OVERSHOOT TEST FIX STARTS #AAA (Requires BBB below)   
//    atDest = true;
//    atDestDirection = currentDirection;
//    lastDirection = currentDirection;
// SHELF OVERSHOOT TEST FIX ENDS    

  } 
 
  else if( ( gap >= ( slowThresh * -1 ) ) && ( gap <= slowThresh ) ){
    velocity = SLOW;    
  }

  else {
    velocity = FAST;      
  } 

  if( velocity != lastVelocity ){

// SHELF OVERSHOOT TEST FIX STARTS #BBB (Requires AAA above)
//    // if we are changing direction after reaching the shelf destination offset by one
//    // because we have likely passed the shelf pendant in question, so we don't want to count it again
//    if(( ! atDest ) && ( lastDirection != currentDirection )){
//      if( currentDirection == UP ){
//        currentPosition++;
//      } else {
//        currentPosition--;
//      }
//    }
// SHELF OVERSHOOT TEST FIX ENDS
    
    switch( velocity ){

    case STOP:
      if( debug ) {
        Serial.println("at dest...stop");      
      }
      stopMoving();
      break;

    case SLOW:
      if( debug ) {
        Serial.println("close...go slow");  
      }
      goSlow();
      break;

    case FAST:
      if( debug ) {
        Serial.println("far...go fast");  
      }
      goFast();
      break;

    }
    lastVelocity = velocity;

  }

}


void stopMoving() {
  if( debug ) {
//    Serial.println("STOP");  
  }  
  digitalWrite( START_OUT, DISABLE ); 
  digitalWrite( SLOW_OUT, DISABLE );     
  digitalWrite( FAST_OUT, DISABLE ); 
}


void goSlow() {
  if( debug ) {
//    Serial.println("SLOW");  
  }    
  digitalWrite( FAST_OUT, DISABLE );
  digitalWrite( SLOW_OUT, ENABLE );  
  digitalWrite( START_OUT, ENABLE );  
}


void goMedium() {
  if( debug ) {
//    Serial.println("MEDIUM");  
  }   
  digitalWrite( SLOW_OUT, DISABLE );
  digitalWrite( FAST_OUT, DISABLE );   
  digitalWrite( START_OUT, ENABLE ); 
}


void goFast() {    
  if( debug ) {
//    Serial.println("FAST");  
  }  
  digitalWrite( SLOW_OUT, DISABLE );    
  digitalWrite( FAST_OUT, ENABLE );   
  digitalWrite( START_OUT, ENABLE );   
}




/************************************************
 * 
 * Move the conveyor until we get trigger Absolute Zero Sensor
 * 
 ************************************************/
 
 
void goHome() {
  
  int isHome = ! ( digitalRead( HOME_IN ) );
  masterGap = 0;
  if( ! isHome ) {
    if( debug ) {
      debugShelfData( shelfDestination, shelfPosition, pendantPosition, currentDirection, masterGap );
    }   
    goSlow();
  } else {
    stopMoving();
    shelfDestination = 1;
    shelfPosition = 1;
    pendantPosition = PENDANTS_PER_SHELF;  

    mode = DISABLE_MODE;
    if( debug ) {  
      Serial.println("home");
      debugShelfData( shelfDestination, shelfPosition, pendantPosition, currentDirection, masterGap );
    }          
  }  
  
}

