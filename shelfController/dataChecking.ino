/************************************************
 * 
 * Get and validate data
 * 
 ************************************************/

int checkForData() {

  int data = -1;
  while( Serial.available( ) ){                              // if new shelf destination is received
    data = Serial.read();                        // convert from ascii character to int so '0' == 0...':' == 10, ';' == 11, '<' == 12
    if( debug ) {
      Serial.println( (char) data );
    }
  }

  return data;

}


// What mode does the data put us in
int modeUpdate( int data ) {

  int modeBuffer = DISABLE_MODE;

  if( shelfValidate( data ) ){
    modeBuffer = RUN_MODE;
    if( debug ) {
      Serial.println( "RUN_MODE" );
    }        
  }  

  else if(( data > (int) COMMANDS_BEGIN ) && ( data < (int) COMMANDS_END )) {
    modeBuffer = PRIMARY_CONTROL_MODE;
    if( debug ) {
      Serial.println( "PRIMARY_CONTROL_MODE" );
    }    
  }
  
  else if( data == (int)SEEK_HOME ){
    modeBuffer = SEEK_HOME_MODE;
    if( debug ) {
      Serial.println( "SEEK_HOME_MODE" );
    }  
  }

  else if( debug ) {
    Serial.println( "INVALID COMMAND" );
  }  

  return modeBuffer;

}


// Tell us if it's a shelf or not
int shelfValidate( int data ) {

  int response = 0;
  int shelfTest = data - '0';

  if(( shelfTest >= 1 ) && ( shelfTest <= SHELF_TOTAL )){  
    response = shelfTest;
  }
  return response;

}
