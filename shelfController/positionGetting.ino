

/************************************************
 * 
 * Find Position 
 * 
 ************************************************/


int shelfPositionUpdate( int shelf ) {

  shelf += shelfRead();  
  shelf = calibrate( shelf, SHELF_TOTAL, SHELF_VALUE ); 
  
  return shelf;  

}


int pendantPositionUpdate( int pendant ) {

  pendant += pendantRead();
  pendant = calibrate( pendant, PENDANT_TOTAL, PENDANTS_PER_SHELF );
  
  return pendant;  

}


// Read the Shelf sensor
int shelfRead() {

  static boolean lastIsShelfThere = false;
  int shelf = 0;

  boolean isShelfThere = ! digitalRead( SHELF_IN );         // invert so the isShelfThere answers true/false correctly

  if( isShelfThere != lastIsShelfThere ) {                  // if there's a change of state...eg. a new shelf
    lastIsShelfThere = isShelfThere;

    if( isShelfThere == true ) {
      if( currentDirection == UP ){
        shelf = 1;                                          // if rotating up then add a shelf
      }
      else {
        shelf = -1;    
      }
    }
  }  
  return shelf;

}


// Read the pendant sensor
int pendantRead() {

  static boolean lastIsPendantThere = false;
  int pendant = 0;   

  boolean isPendantThere = ! digitalRead( PENDANT_IN );

  if( isPendantThere != lastIsPendantThere ) {              // if there's a change of state...eg. a new pendant
    lastIsPendantThere = isPendantThere;

    if( isPendantThere == true ) {
      pendant = 1;
      if( currentDirection == DOWN ){ 
        pendant = -1;      
      }       
    }    
  }
  return pendant;  

}



// if wrapping around then we should hit the Absolute Zero Sensor
int calibrate( int unit, int totalUnits, int unitDivision ) {
  
  int isHome = ! ( digitalRead( HOME_IN ) );
  if( isHome ) {
    unit = unitDivision;
    if( debug ) {    
      Serial.print( "HIT HOME = " );
      Serial.println( unit);
    }    
  }    
  
  // extra calibration in case...
  unit = wrap( unit, totalUnits );
  return unit;

}



// wrap the shelf or pendant number when it rotates past it's max or min
int wrap( int unit, int totalUnits ){

  if( unit > totalUnits ) {
    unit = 1;
  }
  if( unit < 1 ){
    unit = totalUnits;     
  }
  return unit;

}


// Use to compare the shelf and the pendant sensor to make sure they are properly aligned
// Just prints warning. Doesn't currently affect the behaviour of the machine.
void confirm( int shelf, int pendant ) {

  int pendantShelf = pendant / PENDANTS_PER_SHELF;
  if( shelf != pendantShelf ) {
    if( debug ){
      Serial.print( "Warning - " );
      Serial.print( "Tracking Alignment Error: " );
      Serial.print( "Shelf and Pendants " );
      Serial.println( "not aligned" );  
      Serial.println("");    
      delay(100);
    }
  }

}



/************************************************
 * 
 * Figure out the gap between position and destination
 * 
 ************************************************/

int readGap( int dest, int pos, int unitTotal ) {

  static int lastGap;

  int gap = dest - pos;

  if( ONLY_UP ) {
    gap = overflowShift( gap, unitTotal );  
  }
  else {
    gap = directionalShift( gap, unitTotal );
  }
  
  if(debug) {
    static int lastGap;
    if(gap != lastGap){
      Serial.print("d: ");
      Serial.print(dest);
      Serial.print(", p: ");
      Serial.print(pos);
      Serial.print(", gap = ");
      Serial.println(gap);
      lastGap = gap;
    }
  }
  
  return gap;

}


// use only if going in both directions
int directionalShift( int gap, int unitTotal ) {

  if( gap > ( unitTotal / 2 )) { 
    gap = ( unitTotal - gap ) * -1;   
  }
  else if( gap < ( unitTotal / -2 )) {
    gap = gap + unitTotal;  
  }

  return gap;

}


// use only if only going up
int overflowShift( int gap, int unitTotal ) {
  
  if( gap < 0 ){
    gap = gap + unitTotal;  
  }

  return gap;
    
}


// if the shelf is above the dest, move down 
// if the shelf is below the dest, move up
boolean directionUpdate( int gap, boolean dir ) {
  
  if( gap < 0 ) {
    dir = DOWN;
  }
  else if( gap > 0 ){
    dir = UP; 
  }

  return dir;

}


// SHELF OVERSHOOT FIX
// if we change directions then offset the position by one (if we aren't on shelf)
// needed because we slightly pass the shelf, we don't land on it.
// not a problem if we move in same direction, but needs the offset if we change directions.

int calibrateShelvesForDirectionChange( int gap, int dir ){

  static int lastDirection;
  int offset = 0;
  
  boolean isShelf = ! ( digitalRead( SHELF_IN ) );

  // adjust shelf position if switch dir past the shelf
  if(( gap < 0 ) && ( dir != lastDirection ) && ( ! isShelf )) {
    lastDirection = dir;
    offset++;
    if(debug) {
      Serial.println("changed DOWN, shelf++");      
    }
  }
  else if(( gap > 0 ) && ( dir != lastDirection ) && ( ! isShelf )) {
    lastDirection = dir;
    offset--;
    if(debug) {
      Serial.println("changed UP, shelf--");      
    }
  }
  return offset;
}


// PENDANT OVERSHOOT FIX
// if we change directions then offset the position by one (if we aren't on pendant)
// needed because we slightly pass the pendant, we don't land on it.
// not a problem if we move in same direction, but needs the offset if we change directions.

int calibratePendantsForDirectionChange(int dir){

  static int lastDirection;
  int offset = 0;
  boolean isPendant = ! ( digitalRead( PENDANT_IN ) );

  // adjust pendant position if switch dir past the pendant    
  if( dir == DOWN ) { 
    if(( dir != lastDirection ) && ( ! isPendant )){
      offset++;
      lastDirection = dir;
//      if(debug) {
//        Serial.println("change to DOWN with p++");      
//      }
    }     
  }
  else if( dir == UP ){
    if(( dir != lastDirection ) && ( ! isPendant )){
      offset--;
      lastDirection = dir;      
//      if(debug) {
//        Serial.println("change to UP with p--");      
//      }
    }         
  }    

  return offset;
}


// Provide current shelf position as feedback. Report 0 if in between shelves.
void reportShelfFromPendants(){

  static int lastPos;
  
  static int lastShelfReported;
  int shelfToReport;
  
  if(( pendantPosition % PENDANTS_PER_SHELF ) != 0 ){
    shelfToReport = 0;
  }  
  else if(( pendantPosition / PENDANTS_PER_SHELF ) != ( lastPos / PENDANTS_PER_SHELF )) {
    shelfToReport = pendantPosition / PENDANTS_PER_SHELF;
  }

  boolean isShelfThere = ! digitalRead( SHELF_IN );
  
  if(! isShelfThere){
    shelfToReport = 0;
  }  
  else if(shelfPosition != lastPos) {
    shelfToReport = shelfPosition;
  }
  
  if( shelfToReport != lastShelfReported ) {
    Serial.println( shelfToReport ); 
    lastShelfReported = shelfToReport;
  }  
  
}


// Provide current shelf position as feedback. Report 0 if in between shelves.
void reportShelfPosition(int shelf){

  static int lastPos, lastShelfReported;
  int shelfToReport;
  
  boolean isShelfThere = ! digitalRead( SHELF_IN );
  
  if(! isShelfThere){
    shelfToReport = 0;
  }  
  else if(shelf != lastPos) {
    shelfToReport = shelf;
  }
  
  if( shelfToReport != lastShelfReported ) {
    Serial.println( shelfToReport ); 
    lastShelfReported = shelfToReport;
  } 

}



/************************************************
 * 
 * Print out debug information about the position
 * 
 ************************************************/


void debugShelfData( int dest, int pos, int pen, int dir, int gap ) {

  if( ( lastShelfDestination != dest ) || ( lastShelfPosition != pos ) || ( lastPendantPosition != pen ) || ( lastGap != gap ) || ( lastDirection != dir ) ) {
    Serial.print('\t');
    Serial.print("dest = ");
    Serial.print(shelfDestination);
//    Serial.print(" (");
//    Serial.print(shelfDestination * PENDANTS_PER_SHELF);    
//    Serial.print(")"); 
    Serial.print(", pos = ");     
    Serial.print(shelfPosition);       
//    Serial.print(" (");
//    Serial.print(pendantPosition);   
//    Serial.print(")"); 
    Serial.print(", gap = ");  
    Serial.print(masterGap);      
    if( currentDirection == UP ) {
      Serial.print( ", UP" );
    }
    else {
      Serial.print( ", DOWN" );
    }
    Serial.println("");

    lastShelfDestination = shelfDestination;
    lastShelfPosition = shelfPosition;
    lastPendantPosition = pendantPosition;
    lastGap = masterGap;
    lastDirection = currentDirection;
  }  

}

